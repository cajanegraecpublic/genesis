<nav class="navbar navbar-default navbar-fixed-top">
	<div class = 'container-fluid'>
		<div class = 'row' id = 'header'>
			<div class = ' col-md-offset-1 col-md-1 col-xs-3'>
				<a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle txt-upper">
					<i class="fa fa-bars"></i> Menú
				</a>
				<nav id="sidebar-wrapper">
					<a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle txt-blanco">
						<i class="fa fa-bars"></i>
					</a>
					<ul class="sidebar-nav mt-100">
						<li class="">
							<a href = "<?php echo site_url("frontend/index"); ?>">Fondo Horizonte</a>
						</li>
						<li>
							<a href = "<?php echo site_url("frontend/index"); ?>#videos">Videos</a>
						</li>
						<li>
							<a href="http://www.fondosgenesis.com" target = 'blank' class="text-uppercase">AFP Genesis</a>
						</li>
						<li>
							<a href="#">Blog Contenido</a>
						</li>
						<li>
							<a href="#contacto">Contacto</a>
						</li>
					</ul>
				</nav>
			</div>
			<div class = 'col-xs-5 col-xs-offset-4 col-md-offset-7 col-md-1  '>
				<div class = 'align-center txt-blue logo pt-10'>
					<a class="isf-logo" href = 'http://www.fondosgenesis.com/GENESIS/index.php' target = 'blank'></a>
				</div>				
			</div>

		</div>
		<div class = 'row bkg-gris ml-0 pl-0 pt-5 pb-5'>
			<div class = 'col-md-3 '>
				<hr class = 'hr-orange ml-0 pl-0 mt-5 mb-5'>
			</div>
		</div>
	</div>
</nav>