<section id = 'asistir' class = 'section'>
		<div class = 'container-fluid pt-0 mt-0 pl-0 pr-0'>
		<div class = 'container pt-30 pb-50'>
			<div class = 'row'>
				<div class = 'col-md-5'>
					<p class = 'txt-upper txt-orange mb-0'>Te asistimos cuando</p>
					<p class = 'txt-upper txt-orange txt-bold mt-0'>más lo necesitas</p>
					<p class = 'txt-blue mb-0'>Con SOLUCIONA tu puedes solicitar la asistencia, </p>
					<p class = 'txt-blue txt-bold mt-0'>en cualquier momento durante una eventualidad.</p>
				</div>
			</div>
			
			<div class = 'row pt-20'>
				<div class = 'col-md-offset-1 col-md-1 pr-0  hidden-sm hidden-xs'>
					<div class = 'row'>
						<div class = 'col-sm-offset-6 col-sm-6'>
							<div class = 'mr-0 pr-0 video-bkg'></div>
						</div>
					</div>
					
				</div>
				<div class = 'col-md-8 pr-0'>
					<div class = 'pt-10 pb-10 pl-10 pr-10 youtube-vid'>
						<div class="embed-responsive  embed-responsive-16by9">
							<iframe width="853" height="480" src="https://www.youtube.com/embed/8XTqBrMc7BE?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>		
				</div>
				<div class = 'col-md-1  hidden-sm hidden-xs'>
					<div class = 'row  '>
						<div class = 'col-sm-6 '>
							<div class = 'video-bkg'></div>
						</div>
					</div>
				</div>
			</div>
			<div class = 'row pt-0'>
				<div class = 'col-md-offset-2 col-md-8'>
					<p class = 'txt-bold txt-orange align-center'> 
						<a class = 'dwl' href = "<?php echo base_url('public/Cuadernillo_Soluciona.pdf'); ?>" target = 'blank'><span class = 'glyphicon glyphicon-download-alt' aria-hidden ='true'></span>
						Guía Completa de los Servicios Integrales de Asistencia Soluciona</a>
					</p>
				</div>
			</div>
		</div>
		</div>
	</section>