	<footer class = 'bkg-gris mb-0 pb-0'>
		<nav class="navbar navbar-default mb-0">
			<div class="container">
				<div class = 'row'>
					<div class = 'col-md-offset-10 col-md-2 col-xs-6'>
						<div class = 'txt-upper pt-5'>
							<a id = 'gotop' href = '<?php echo site_url("Frontend/index"); ?>'>Ir al inicio</a> |
							<a id = 'govideos' href = '<?php echo site_url("Frontend/index"); ?>#videos'>Videos</a>
						</div>	
					</div> 
				</div>
			</div>
		</nav>
	</footer>


        <script type="text/javascript" src="<?php echo base_url('public/frontend/js/jquery.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('public/frontend/js/bootstrap.min.js'); ?>"></script>

        <script type="text/javascript" src="<?php echo base_url('public/frontend/js/made.js'); ?>"></script>
        <script src="<?php echo base_url('public/frontend/js/jquery.smartscroll.js'); ?>"></script>


</body>
</html>