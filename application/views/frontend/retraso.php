<section id = 'retraso' class = 'section'>
		<div class = 'container-fluid pt-0 mt-0 pl-0 pr-0'>
		<div class = 'container pt-30 pb-50'>
			<div class = 'row'>
				<div class = 'col-md-4'>
					<p class = 'txt-upper'>No retrases tus sueños</p>
					<p class = 'txt-bold'>Te explicamos cómo te afecta no haber realizado tu primer aporte</p>
				</div>
			</div>
			<div class = 'row pt-20'>
				<div class = 'col-md-offset-1 col-md-1 pr-0  hidden-sm hidden-xs'>
					<div class = 'row'>
						<div class = 'col-sm-offset-6 col-sm-6'>
							<div class = 'mr-0 pr-0 video-bkg'></div>
						</div>
					</div>
					
				</div>
				<div class = 'col-md-8 pr-0'>
					<div class = 'pt-10 pb-10 pl-10 pr-10 youtube-vid'>
						<div class="embed-responsive  embed-responsive-16by9">
							<iframe width="853" height="480" src="https://www.youtube.com/embed/8wjOpSyLdf8?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>		
				</div>
				<div class = 'col-md-1  hidden-sm hidden-xs'>
					<div class = 'row  '>
						<div class = 'col-sm-6 '>
							<div class = 'video-bkg'></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</section>