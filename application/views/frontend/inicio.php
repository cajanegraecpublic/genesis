
<section id = "top" class = 'pt-60'>
	<div class = 'container-fluid pb-0 mb-0 pl-0 pr-0'>
		<div class = 'row ml-0 mr-0 pl-0 pr-0'>
			<div class = 'container'>
				<div class = 'col-md-offset-3 col-md-3 col-sm-offset-3 col-sm-6 col-xs-offset-1 col-xs-10 '>
					<div class = 'title-init'>
						<p class = "mb-0 ml-0 txt-white welcome-1">Fondo Horizonte</p>
					
						<p class = "mb-0 ml-0 txt-white welcome-1">Construyendo juntos</p>
						<p class = "mb-0 ml-0 txt-white welcome-3">El futuro que te mereces</p>
						<p class = "mb-0 ml-0 txt-white init-descp">Junto a GENESIS formarás parte de más de 296.000 ecuatorianos que han confiado su futuro a los expertos. ¡Te damos la Bienvenida!</p>
					</div>
				</div>
			</div>
			<div class = 'align-center pt-0 mt-0'>
				<img src = "<?php echo base_url('public/frontend/img/init.png'); ?>" class = 'wlk-img'>
			</div>
		</div>
		</div>
	</section>


	<section id = 'videos' class = 'section'> 
	<div class = 'container-fluid pt-0 mt-0 pl-0 pr-0'>
		<div class = 'container pb-50'>
			<div class = 'row pl-xs-5'>
				<h1>Videos</h1>
				<h5>Explora los videos informativos: </h5>
			</div>
			<div class="row">
				<?php foreach ($datos as $dato): ?>
				<div class = 'col-md-3 mb-10'>
					<h6 class = 'align-center'><?php echo $dato['titulo_index']; ?></h6>
					<?php if ( $dato['estado'] == 1 ): ?>
						<a href = "<?php echo site_url('Frontend/seccion/' . $dato['id']); ?>" class = 'hvr-outline-out'>
							<img src="<?php echo base_url('assets/genesis/publicacion/' . $dato['imagen']); ?>" class = ' align-center img-responsive ' />
							<span class="isf-play play-icon"></span>
						</a>
					<?php else: ?>
						<div class = 'hvr-outline-out'>
							<img src="<?php echo base_url('public/frontend/img/exequial.jpg'); ?>" class = ' align-center img-responsive '>	
							<span class="isf-play play-icon"></span>
						</div>
					<?php endif ?>
				</div>
				<?php endforeach; ?>
			</div>

			<?php /* 
			<div class = 'row'>
				<div class = ' col-md-offset-1 col-md-3'>
					<h6 class = 'align-center'>Bienvenido a tu fondo horizonte</h6>
					<a href = "<?php echo site_url('Frontend/bienvenida'); ?>" class = 'hvr-outline-out'>
						<img src="<?php echo base_url('public/frontend/img/seccion1-1.jpg'); ?>" class = ' align-center img-responsive '>
						<span class="isf-play play-icon"></span>
					</a>

										
				</div>
				<div class = 'col-md-3'>
					<h6 class = 'align-center'>Felicidades por tu primer aporte</h6>
					<a href = "<?php echo site_url('Frontend/primerpaso'); ?>" class = 'hvr-outline-out'>
						<img src="<?php echo base_url('public/frontend/img/seccion1-2.jpg'); ?>" class = ' align-center img-responsive '>	
						<span class="isf-play play-icon"></span>	
						<?php // <span class = 'pronto'>Muy pronto conocerás más de este servicio</span> ?>
					</a>		
				</div>
				<div class = 'col-md-3'>
					<h6 class = 'align-center'>Aún no realizas tu primer aporte</h6>
					<a href = "<?php echo site_url('Frontend/retraso'); ?>" class = 'hvr-outline-out'>
						<img src="<?php echo base_url('public/frontend/img/seccion1-3.jpg'); ?>" class = ' align-center img-responsive '>
						<span class="isf-play play-icon"></span>	
						<?php // <span class = 'pronto'>Muy pronto conocerás más de este servicio</span> ?>
					</a>				
				</div>
			</div>
			<div class = 'row pt-30'>
				<div class = 'col-md-3'>
					<h6 class = 'align-center'>Seguro global</h6>
					<a href = "<?php echo site_url('Frontend/proteger'); ?>" class = 'hvr-outline-out'>
						<img src="<?php echo base_url('public/frontend/img/seccion1-4.jpg'); ?>" class = ' align-center img-responsive '>	
						<span class="isf-play play-icon"></span>
						<?php // <span class = 'pronto'>Muy pronto conocerás más de este servicio</span> ?>
					</a>				
				</div>
				<div class = 'col-md-3'>
					<h6 class = 'align-center'>Soluciona</h6>
					<a href = "<?php echo site_url('Frontend/asistir'); ?>" class = 'hvr-outline-out'>
						<img src="<?php echo base_url('public/frontend/img/seccion1-5.jpg'); ?>" class = ' align-center img-responsive '>
						<span class="isf-play play-icon"></span>
						<?php // <span class = 'pronto'>Muy pronto conocerás más de este servicio</span> ?>
					</a>				
				</div>
				<div class = 'col-md-3'>
					<h6 class = 'align-center'>Asistencia exequial extendida</h6>

					<?php
						// cuando se active poner en un a href y el link es ese 
						//echo site_url("Frontend/juntoati");
					?>
					<div class = 'hvr-outline-out'>
						<img src="<?php echo base_url('public/frontend/img/exequial.jpg'); ?>" class = ' align-center img-responsive '>	
						<span class="isf-play play-icon"></span>	
						<?php // <span class = 'pronto'>Muy pronto conocerás más de este servicio</span> ?>
					</div>			
				</div>
				<div class = 'col-md-3'>
					<h6 class = 'align-center'>Sistema alternativo de cobro de prima</h6>
					<a href = "<?php echo site_url('Frontend/tranquilidad'); ?>" class = 'hvr-outline-out'>
						<img src="<?php echo base_url('public/frontend/img/seccion1-7.jpg'); ?>" class = ' align-center img-responsive '>	
						<span class="isf-play play-icon"></span>	
						<?php // <span class = 'pronto'>Muy pronto conocerás más de este servicio</span> ?>
					<a/>			
				</div>
			</div>
			*/ ?>

		</div>

		</div>
	</section>

	