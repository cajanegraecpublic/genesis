	<section id = 'juntoati' class = 'section'>
		<div class = 'container-fluid pt-0 mt-0 pl-0 pr-0'>
		<div class = 'container pt-30 pb-50'>
			<div class = 'row'>
				<div class = 'col-md-5'>
					<p class = 'title-video txt-normal'>Existen eventos que no podemos cambiar,</p>
					<p class = 'title-video txt-bold'>Genesis está junto a ti, cuando más lo necesitas</p>
				</div>
			</div>
			
			<div class = 'row pt-20'>
				<div class = 'col-md-offset-2 col-md-8'>
					<p class = 'txt-bold txt-blanc txt-upper'>Asistencia Exequial Extendida</p>
				</div>
			</div>
			<div class = 'row pt-0'>
				<div class = 'col-md-offset-1 col-md-1 pr-0  hidden-sm hidden-xs'>
					<div class = 'row'>
						<div class = 'col-sm-offset-6 col-sm-6'>
							<div class = 'mr-0 pr-0 video-bkg'></div>
						</div>
					</div>
					
				</div>
				<div class = 'col-md-8 pr-0'>
					<div class = 'pt-10 pb-10 pl-10 pr-10 youtube-vid'>
						<div class="embed-responsive  embed-responsive-16by9">
							<iframe width="853" height="480" src="https://www.youtube.com/embed/EbesSNF57Z8?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>		
				</div>
				<div class = 'col-md-1  hidden-sm hidden-xs'>
					<div class = 'row  '>
						<div class = 'col-sm-6 '>
							<div class = 'video-bkg'></div>
						</div>
					</div>
				</div>
			</div>
			<div class = 'row pt-0'>
				<div class = 'col-md-offset-2 col-md-8'>
					<p class = 'txt-bold txt-blanc align-center'> 
						<a class = 'dwl' href = "<?php echo base_url('public/Cuadernillo_Seguro_Global.pdf'); ?>" target = 'blank'><span class = 'glyphicon glyphicon-download-alt' aria-hidden ='true'  ></span>
						Descarga la Guía Completa de tu Seguro Global</a>
					</p>
				</div>
			</div>
		</div>
		</div>
	</section>