<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Genesis</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

         <!-- Bootstrap Core CSS -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/frontend/css/bootstrap.min.css'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/frontend/css/responsive.css'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/frontend/css/custom-bootstrap-margin-padding.css'); ?>">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,700" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/frontend/fonts/stylesheet.css'); ?>" >
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/frontend/css/made.css'); ?>" >

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/frontend/fonts/font-awesome.min.css'); ?>">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
        

        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url('public/frontend/favicon/apple-icon-57x57.png') ?>">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('public/frontend/favicon/apple-icon-60x60.png') ?>">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('public/frontend/favicon/apple-icon-72x72.png') ?>">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('public/frontend/favicon/apple-icon-76x76.png') ?>">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('public/frontend/favicon/apple-icon-114x114.png') ?>">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url('public/frontend/favicon/apple-icon-120x120.png') ?>">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url('public/frontend/favicon/apple-icon-144x144.png') ?>">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('public/frontend/favicon/apple-icon-152x152.png') ?>">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('public/frontend/favicon/apple-icon-180x180.png') ?>">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url('public/frontend/favicon/android-icon-192x192.png') ?>">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('public/frontend/favicon/favicon-32x32.png') ?>">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('public/frontend/favicon/favicon-96x96.png') ?>">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('public/frontend/favicon/favicon-16x16.png') ?>">
        <link rel="manifest" href="<?php echo base_url('public/frontend/favicon/manifest.json"') ?>">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
		</head>
    <body>