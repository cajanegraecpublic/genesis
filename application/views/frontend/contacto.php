<section id = 'contacto' class = 'section pb-50'>
		<div class = 'container-fluid pt-0 mt-0 pl-0 pr-0'>
		<div class = 'container pt-30 pb-50'>
			<div class = 'row'>
				<div class = 'col-md-offset-1 col-md-3'>
					<img src = "<?php echo base_url('public/frontend/img/call-center.png'); ?>"  class = 'img-responsive'>
				</div>
				<div class = 'col-md-6' >
					<p class = 'contacto-dudas'>¿Tienes dudas o sugerencias?</p>
					<p class = 'contacto-dudas'>Escríbenos</p>
					<form class="form-horizontal txt-blanc pt-10" method="POST" action="<?php echo site_url('frontend/enviarmail'); ?>" >
						<div class="form-group">
							<label for="inputName" class="col-sm-2 control-label">Nombre:</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="inputName" id="inputName" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="inputMail" class="col-sm-2 control-label">eMail:</label>
							<div class="col-sm-10">
								<input type="email" class="form-control" name="inputMail" id="inputMail" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="inputTopic" class="col-sm-2 control-label">Asunto:</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="inputTopic" id="inputTopic" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="inputMsg" class="col-sm-2 control-label">Mensaje:</label>
							<div class="col-sm-10">
								<textarea class="form-control" name="inputMsg" id='inputMsg' rows="3"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-9 col-sm-3">
								<button type="submit" class="btn btn-defaul">Enviar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class = 'row'>
				<h4><a href = 'http://www.fondosgenesis.com' target = 'blank'>www.fondosgenesis.com</a></h4>
					<p> <a href = 'https://www.facebook.com/FondosGenesis' target = 'blank'><span class = 'fbfacebook2'></span> Genesis Administradora de Fondos</a></p>
			</div>
			<div class = 'row mt-10'>
				<div class = 'col-md-offset-2  col-md-4'>
					<h4>Fondo Horizonte</h4>
					<p>Línea de atención al cliente 1800 237 237</p>
					<p>E-mail: <a href = 'mailto:serviciosalcliente@fondosgenesis.com'>serviciosalcliente@fondosgenesis.com</a></p>
				</div>
				<div class = 'col-md-4'>
					<h4>Fondos de inversión</h4>
					<p>Guayaquil: (04) 2887690</p>
					<p>Quito: (02) 2277996</p>
					<p>E-mail: <a href = 'mailto:fondosdeinversion@fondosgenesis.com'>fondosdeinversion@fondosgenesis.com</a></p>
				</div>
			</div>
			<div class = 'row mt-10'>
				<div class = 'align-center'>
					<p>Para las Asistencias comunícate: </p>
				</div>
			</div>
			<div class = 'row mt-10'>
				<div class = 'col-md-offset-2 col-md-3'>
					<p class = 'soluciona'>Soluciona</p>
					<p>1800 222 111</p>
					<p>0999494488 / 0994502501</p>
					<p>(02) 2990775 / (02) 2554788 / (02) 2554450</p>
				</div>
				<div class = 'col-md-2'>
					<p class = 'soluciona'>Asistencia Exequial</p>
					<p>1800 244 766</p>
					<p>0996275604</p>
					<p>(02) 2550290</p>
					<p>(02) 2550043</p>
				</div>
				<div class = 'col-md-3'>
					<p class = 'soluciona'>Global Salud</p>
					<p>1800 773836</p>
					<p>(02) 2250911</p>
					<p>(02) 2250910</p>
				</div>
			</div>
		</div>
		</div>
	</section>