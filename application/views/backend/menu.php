<!-- Navigation -->
    <nav class="navbar navbar-cdra navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo site_url('backend/index'); ?>">
                <img class="img-responsive obj-centrar img-logoadmin" src="<?php echo base_url('public/frontend/img/logo.svg'); ?>">
            </a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i>
                    <span class="capitalize">
                        <?php echo $this->session->nombr; ?>
                    </span>
                    <span>
                        (<?php echo $this->session->correo; ?> )
                    </span>
                    <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li class="divider"></li>
                    <li>
                        <a href="<?php echo site_url('backend/logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> Cerrar sesión</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="<?php echo site_url('backend/index'); ?>">
                            <i class="fa fa-dashboard fa-fw"></i> Inicio
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('backend/publicaciones'); ?>">
                            Publicaciones
                        </a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->

    </nav>
