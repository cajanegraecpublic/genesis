<?php
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    /**
     * Description of Security
     *
     * @author Hector
     */
    class SecurityUser extends CI_Model {

        var $nombre = "";
        var $correo = "";
        var $password = "";
        var $fecha_creacion = "";
        var $estado = "";
        
        function __construct() {
            parent::__construct();
            $this->load->database();
            $this->load->library('session');
        }

        function login($correo, $password) {
            $usuario = $this->db->get_where("usuario", array('correo'=> $correo , 'password' => md5($password)))->row();  
          
            if ($usuario) {
                $data_user = array("correo" => $usuario->correo, "nombre" => $usuario->nombre); 
                $this->session->set_userdata($data_user);
                return true;
            }
        }
        
        function logout(){        
            $this->session->sess_destroy();        
        }

    }
?>
