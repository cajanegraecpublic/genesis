<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Frontend extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $this->load->library('email');

        $this->load->database();

        date_default_timezone_set("America/Guayaquil");
    }


     public function index(){
         $titulo = "Genesis";
        $dataHeader['titlePage'] = $titulo;

        $datos = $this->db->get_where("publicacion")->result_array();

        $data['header'] = $this->load->view('frontend/header', $dataHeader);
        $data['menu'] = $this->load->view('frontend/menu', array() );

        $data['contenido'] = $this->load->view('frontend/inicio', array("datos" => $datos));
        $data['contacto'] = $this->load->view('frontend/contacto', array() );
        $data['footer'] = $this->load->view('frontend/footer', array() );
     }

     public function bienvenida(){
        $titulo = "Genesis";
        $dataHeader['titlePage'] = $titulo;

        $data['header2'] = $this->load->view('frontend/header2', $dataHeader);
        $data['menu2'] = $this->load->view('frontend/menu2', array() );

        $data['bienvenida'] = $this->load->view('frontend/bienvenida', array() );
        $data['contacto'] = $this->load->view('frontend/contacto', array() );
        $data['footer2'] = $this->load->view('frontend/footer', array() );
     }

     public function seccion() {
      $id = $this->uri->segment(3);
      if ($id) {
        $datos = $this->db->get_where("publicacion", array('id'=> $id, "estado" => 1))->row();
        if ($datos) {
          $titulo = "Genesis";
          $dataHeader['titlePage'] = $titulo;

          $data['header2'] = $this->load->view('frontend/header2', $dataHeader);
          $data['menu2'] = $this->load->view('frontend/menu2', array());

          $data['contenido'] = $this->load->view('frontend/seccion', $datos);
          $data['contacto'] = $this->load->view('frontend/contacto', array());
          $data['footer2'] = $this->load->view('frontend/footer', array());
        } else {
          redirect('frontend/index');
        }
      } else {
        redirect('frontend/index');
      }

     }

     public function primerpaso(){
        $titulo = "Genesis";
        $dataHeader['titlePage'] = $titulo;

        $data['header2'] = $this->load->view('frontend/header2', $dataHeader);
        $data['menu2'] = $this->load->view('frontend/menu2', array() );

        $data['primerpaso'] = $this->load->view('frontend/primerpaso', array() );
        $data['contacto'] = $this->load->view('frontend/contacto', array() );
        $data['footer2'] = $this->load->view('frontend/footer', array() );
     }

     public function retraso(){
        $titulo = "Genesis";
        $dataHeader['titlePage'] = $titulo;

        $data['header2'] = $this->load->view('frontend/header2', $dataHeader);
        $data['menu2'] = $this->load->view('frontend/menu2', array() );

        $data['retraso'] = $this->load->view('frontend/retraso', array() );
        $data['contacto'] = $this->load->view('frontend/contacto', array() );
        $data['footer2'] = $this->load->view('frontend/footer', array() );
     }

     public function proteger(){
        $titulo = "Genesis";
        $dataHeader['titlePage'] = $titulo;

        $data['header2'] = $this->load->view('frontend/header2', $dataHeader);
        $data['menu2'] = $this->load->view('frontend/menu2', array() );

        $data['proteger'] = $this->load->view('frontend/proteger', array() );
        $data['contacto'] = $this->load->view('frontend/contacto', array() );
        $data['footer2'] = $this->load->view('frontend/footer', array() );
     }

     public function asistir(){
        $titulo = "Genesis";
        $dataHeader['titlePage'] = $titulo;

        $data['header2'] = $this->load->view('frontend/header2', $dataHeader);
        $data['menu2'] = $this->load->view('frontend/menu2', array() );

        $data['asistir'] = $this->load->view('frontend/asistir', array() );
        $data['contacto'] = $this->load->view('frontend/contacto', array() );
        $data['footer2'] = $this->load->view('frontend/footer', array() );
     }

      public function juntoati(){
        $titulo = "Genesis";
        $dataHeader['titlePage'] = $titulo;

        $data['header2'] = $this->load->view('frontend/header2', $dataHeader);
        $data['menu2'] = $this->load->view('frontend/menu2', array() );

        $data['juntoati'] = $this->load->view('frontend/juntoati', array() );
        $data['contacto'] = $this->load->view('frontend/contacto', array() );
        $data['footer2'] = $this->load->view('frontend/footer', array() );
     }

      public function tranquilidad(){
        $titulo = "Genesis";
        $dataHeader['titlePage'] = $titulo;

        $data['header2'] = $this->load->view('frontend/header2', $dataHeader);
        $data['menu2'] = $this->load->view('frontend/menu2', array() );

        $data['tranquilidad'] = $this->load->view('frontend/tranquilidad', array() );
        $data['contacto'] = $this->load->view('frontend/contacto', array() );
        $data['footer2'] = $this->load->view('frontend/footer', array() );
     }

  //Enviar Mail
  public function enviarmail(){
    $idNombre    = $this->input->post("inputName");
    $idAsunto    = $this->input->post("inputTopic");
    $idMail      = $this->input->post("inputMail");
    $idMensaje   = $this->input->post("inputMsg");

    /*
    $config['protocol'] = 'sendmail';
    $config['mailpath'] = '/usr/sbin/sendmail';
    $config['wordwrap'] = TRUE;
    */

    $config['protocol']  = 'smtp';
    $config['smtp_host'] = '****************'; // URL DEL HOST SMTP
    $config['smtp_port'] = 000; // PUERTO SMTP DEL CLIENTE (va sin comillas)
    $config['smtp_user'] = '****************'; // CORREO SMTP
    $config['smtp_pass'] = '****************'; // CONTRASEÑA DEL EMAIL
    $config['charset']   = 'utf-8';
    $config['mailtype']  = 'html';

    

    $this->email->set_newline("\r\n");
    $this->email->set_crlf("\r\n");

    $mailDestinatarios = '****************'; // EMAIL DESTINATARIO o (Web Master)
    ////////////////////////////////////////////////////////////////////////////
    // Mensaje para admin
    ////////////////////////////////////////////////////////////////////////////
    $correo_mensaje = "<html>
                <body>
                  <div style='width:650px;height:70px;background-color:#333;'>
                    <div style='display:block;height:auto;width:27%;'>
                      <img style='margin: 0 auto; display:block;max-width:100%;' src='" . base_url('public/frontend/img/call-center.png') . "'>
                    </div>
                  </div>";
    $correo_mensaje .= "<p>Se ha recibido un mensaje con los siguientes datos.</p><br>";
    $correo_mensaje .= "<strong>Nombre: </strong>" . $idNombre . "<br>";
    $correo_mensaje .= "<strong>Mail: </strong>" . $idMail . "<br>";
    $correo_mensaje .= "<strong>Asunto: </strong>" . $idAsunto . "<br>";
    $correo_mensaje .= "<strong>Mensaje: </strong>" . $idMensaje . "<br>";
    $correo_mensaje .= "</body>
              </html>";

    $this->email->initialize($config);

    $this->email->from('info@fondosgenesis.com', 'Info Génesis');
    $this->email->to( $mailDestinatarios );
    $this->email->subject("Solicitud de Información recibida");
    $this->email->message($correo_mensaje);

    if ( $this->email->send() ) {
      redirect('/frontend/index/', 'refresh');
    }else{
      // aquí si es que no se envía el email.
      //show_error($this->email->print_debugger());
      //return false;

      // si da error igual mandamos al home para que no se vea feo
      redirect('/frontend/index/', 'refresh');

    }
  }

}