<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backend extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $this->load->model('securityuser');
		$this->load->library('grocery_CRUD');
		date_default_timezone_set("America/Guayaquil");
    }

    public function index() {
    	if ($this->securityCheck()) {
	        $titulo = "Genesis";
	        $dataHeader['titlePage'] = $titulo;

	        $data['header'] = $this->load->view('backend/header', $dataHeader);
	        $data['menu'] = $this->load->view('backend/menu', array());

	        $data['contenido'] = $this->load->view('backend/index', array());
	        $data['footer'] = $this->load->view('backend/footer', array());
	    } else {
	    	redirect("backend/login");
	    }
    }

    public function login() {
    	if ($this->securityCheck()) {
    		redirect("backend/index");
    	} else {
	    	$titulo = "Genesis";
	        $dataHeader['titlePage'] = $titulo;

	        $data['header'] = $this->load->view('backend/header', $dataHeader);
	        $data['contenido'] = $this->load->view('backend/login', array());
	        $data['footer'] = $this->load->view('backend/footer', array());
    	}
    }

    public function logout() {
    	if ($this->securityCheck()) {
    		$securityUser = new SecurityUser();
			$securityUser->logout();
    		redirect("backend/login");
    	} else {
    		redirect("backend/login");
    	}
    }

    public function autentificar() {
    	$username = $this->input->post("username");
        $password = $this->input->post("password");

        $securityUser = new SecurityUser();

        $securityUser->login($username, $password);

        if ( $this->session->userdata('correo') != "") {
            redirect("backend/index");
        }else{
            redirect("backend/logout");
        }
    }

    function securityCheck() {
		$usuario = $this->session->userdata('correo');
		if ($usuario == "") {
			return false;
		} else {
			return true;
		}
	}

	public function publicaciones() {
		if ($this->securityCheck()) {
            $titulo = "Publicacion";
            
            $crud = new grocery_CRUD();
            $crud->set_table("publicacion");
            $crud->set_subject($titulo);

            $crud->display_as('titulo_index', 'Para pantalla principal');
            $crud->display_as('titulo', 'Título');
            $crud->display_as('titulo_negrita', 'Porción del Título en Negrita');
            $crud->display_as('subtitulo', 'Subtítulo');
            $crud->display_as('imagen', 'Imagen (268px X 160px)');
            $crud->display_as('codigo_video', 'Código de Vídeo Youtube');
            $crud->display_as('texto', 'Texto');
            $crud->display_as('pdf', 'Archivo PDF');
            $crud->display_as('pdf_titulo', 'Título del PDF');
            $crud->display_as('estado', 'Estado');

            $crud->field_type('estado', 'dropdown', array(
                '0' => 'Inactivo',
                '1' => 'Activo'
            ));

            $crud->set_field_upload('imagen', 'assets/genesis/publicacion');
            $crud->set_field_upload('pdf', 'assets/genesis/pdf');

            $crud->columns('imagen', 'titulo_index', 'titulo', 'titulo_negrita', 'subtitulo', 'codigo_video', 'texto', 'pdf', 'pdf_titulo', 'estado');
            $crud->fields('titulo_index', 'titulo', 'titulo_negrita', 'subtitulo', 'imagen', 'codigo_video', 'texto', 'pdf', 'pdf_titulo', 'estado');
            $crud->required_fields('titulo_index', 'titulo', 'titulo_negrita', 'imagen', 'codigo_video', 'estado');

            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();

            $dataHeader['titlePage'] = $titulo;
            $dataHeader['css_files'] = $output->css_files;
            $dataFooter['js_files'] = $output->js_files;

            
            $data['header'] = $this->load->view('backend/header', $dataHeader);
            $data['menu'] = $this->load->view('backend/menu', $dataHeader);

            $data['content'] = $this->load->view('backend/blank', $output);
            $data['footer'] = $this->load->view('backend/footer-grocerycrud', $dataFooter);
        } else {
            redirect("backend/login");
        }
	}
}