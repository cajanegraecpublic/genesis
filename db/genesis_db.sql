-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 10-03-2017 a las 20:06:11
-- Versión del servidor: 5.6.26
-- Versión de PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `genesis_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicacion`
--

CREATE TABLE IF NOT EXISTS `publicacion` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `titulo_negrita` varchar(100) NOT NULL,
  `titulo_index` varchar(100) NOT NULL,
  `subtitulo` varchar(100) NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `codigo_video` varchar(100) NOT NULL,
  `texto` text NOT NULL,
  `pdf` varchar(500) NOT NULL,
  `pdf_titulo` varchar(200) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `publicacion`
--

INSERT INTO `publicacion` (`id`, `titulo`, `titulo_negrita`, `titulo_index`, `subtitulo`, `imagen`, `codigo_video`, `texto`, `pdf`, `pdf_titulo`, `estado`, `fecha_creacion`) VALUES
(1, '¡BIENVENIDO A GENESIS!', 'DESDE AHORA INICIAREMOS JUNTOS UN CAMINO HACIA UN NUEVO Y MEJOR HORIZONTE', 'BIENVENIDO A TU FONDO HORIZONTE', '', '3f1c7-seccion1-1.jpg', 'EbesSNF57Z8', '', '', '', 1, '2017-03-10 18:19:11'),
(2, '¡HAZ DADO TU PRIMER PASO PARA', '¡QUE TUS PROYECTOS SE HAGAN REALIDAD!', 'FELICIDADES POR TU PRIMER APORTE', 'Aquí te recordamos como opera tu Fondo Horizonte y qué son los ciclos de aportación', '78b21-seccion1-2.jpg', 'T3MseB4OGq4', '', '', '', 1, '2017-03-10 18:20:27'),
(3, 'NO RETRASES TUS SUEÑOS', 'Te explicamos cómo te afecta no haber realizado tu primer aporte', 'AÚN NO REALIZAS TU PRIMER APORTE', '', 'db921-seccion1-3.jpg', '8wjOpSyLdf8', '', '', '', 1, '2017-03-10 18:21:14'),
(4, 'GUSTOSOS DE PROTEGERTE A', 'TI LOS QUE MÁS QUIERES', 'SEGURO GLOBAL', 'Aquí te explicamos más sobre las coberturas de tu Seguro Global', '3ceaf-seccion1-4.jpg', 'wFQNhIrggtc', '', '', '', 1, '2017-03-10 18:22:33'),
(5, 'TE ASISTIMOS CUANDO', 'MÁS LO NECESITAS', 'SOLUCIONA', 'Con SOLUCIONA tu puedes solicitar la asistencia, en cualquier momento durante una eventualidad.', 'b93ee-seccion1-4.jpg', '8XTqBrMc7BE', '', '08b07-cuadernillo_soluciona.pdf', 'Guía Completa de los Servicios Integrales de Asistencia Soluciona', 1, '2017-03-10 18:31:32'),
(6, 'Existen eventos que no podemos cambiar,', 'GENESIS ESTÁ JUNTO A TI, CUANDO MÁS LO NECESITAS', 'ASISTENCIA EXEQUIAL EXTENDIDA', '', '209de-exequial.jpg', 'EbesSNF57Z8', '', '04d4e-cuadernillo_seguro_global.pdf', ' Descarga la Guía Completa de tu Seguro Global', 0, '2017-03-10 18:51:32'),
(7, 'Pensando en tu tranquilidad y bienestar,', 'Tu cuentas con el Sistema Alternativo de Cobro de Prima de Saldo.', 'SISTEMA ALTERNATIVO DE COBRO DE PRIMA', 'Te explicamos en qué consiste', 'a76b9-seccion1-7.jpg', '7679URbxVRg', '', '40dfb-mds_sistema_alternativo_cobro_de_prima.pdf', 'Más sobre el Sistema Alternativo de Cobro de Prima del Saldo', 1, '2017-03-10 18:53:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `correo`, `password`, `estado`, `fecha_creacion`) VALUES
(1, 'Admin', 'fabricio@cajanegra.com.ec', 'e348545fe7d7667aedb123a898d6fd30', 1, '2017-03-09 21:13:16');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `publicacion`
--
ALTER TABLE `publicacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `publicacion`
--
ALTER TABLE `publicacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
